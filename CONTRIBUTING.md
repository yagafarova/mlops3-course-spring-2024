# Contribution guide

## Table of contents

- [Table of contents](#table-of-contents)
- [How to start](#how-to-start)
- [Step-by-step guide](#step-by-step-guide)
  - [Before the PR](#before-the-pr)
  - [New feature](#new-feature)
  - [Bugfix](#bugfix)
- [Contribution best practices](#contribution-best-practices)
    - [Codestyle](#codestyle)
    - [Tests](#tests)


## How to start?

Contributing is quite easy: suggest ideas and make them done.
We use [GitLab issues](https://gitlab.com/yagafarova/mlops3-course-spring-2024/-/issues) for bug reports and feature requests.

Every good PR usually consists of:
- feature implementation :)
- documentation to describe this feature to other people
- tests to ensure everything is implemented correctly


## Step-by-step guide

### Before the PR

Make sure to read CONTRIBUTING.md properly!

### New feature

1. Make an issue with your feature description;
2. We shall discuss the design and its implementation details;
3. Once we agree that the plan looks good, go ahead and implement it.

### Bugfix

1. Goto [GitLab issues](https://gitlab.com/yagafarova/mlops3-course-spring-2024/-/issues);
2. Pick an issue and comment on the task that you want to work on this feature;
3. If you need more context on a specific issue, please ask, and we will discuss the details.


Once you finish implementing a feature or bugfix, please send a Merge Request.

If you are not familiar with creating a Merge Request, here are some guides:
- https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html
- https://nira.com/gitlab-pull-request/


## Contribution best practices

0. Install Python 3.8
    - Windows

        Install with [official executable](https://www.python.org/downloads/)

    - Linux / MacOS

        ```bash
        sudo apt install python3.8-dev
        ```
0. Install poetry

   - Windows

        Use [official instructions](https://python-poetry.org/docs/#windows-powershell-install-instructions) or use `powershell` command:

        ```powershell
        (Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
        ```

   - Linux

        Use [official instructions](https://python-poetry.org/docs/#installing-with-the-official-installer) or bash command:

        ```bash
        curl -sSL https://install.python-poetry.org | python3 -
        ```

0. Install requirements with `poetry install`
    > Or if you prefer different dependency management, you can install requirements using pip: `pip install .`
0. Break your work into small, single-purpose updates if possible.
It's much harder to merge in a large change with a lot of disjoint features.
0. Submit the update as a GitLab merhe request against the `main` branch.
0. Make sure that you provide docstrings for all your new methods and classes.
0. Add new unit tests for your code.
0. Check the [codestyle](#codestyle)..


### Codestyle

You could check the codestyle for your PR with pre-commit hooks, so please run (this requires `pre-commit` package, pinned in the [pyproject.toml](./pyproject.toml)):
    ```bash
    pre-commit install
    ```
    Once the installation is done, all the files that are changed will be formatted automatically (and commit halted if something goes wrong, e.g there is a syntactic error). You can also run the formatting manually:
    ```bash
    pre-commit run
    ```

Once again, make sure that your python packages complied with [pyproject.toml](./pyproject.toml) to get codestyle and pre-commit run clean:
```bash
poetry install
```
or
```bash
pip install .
```

For more information on pre-commit, please refer to  [pre-commit documentation](https://pre-commit.com/).


### Tests

Do not forget to check that your code passes the unit tests:
```bash
pytest .
```
