# mypy: disable-error-code="attr-defined"
"""Repo for homework for the course https://ods.ai/tracks/mlops3-course-spring-2024"""

from importlib import metadata as importlib_metadata


def get_version() -> str:
    try:
        return importlib_metadata.version(__name__)
    except importlib_metadata.PackageNotFoundError:  # pragma: no cover
        return "unknown"


__version__: str = get_version()
