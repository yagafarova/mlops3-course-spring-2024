# MLOps and production in DS research 3.0
Repo with homework of Kseniia Alekseitseva for the course [MLOps и production в DS исследованиях 3.0](https://ods.ai/tracks/mlops3-course-spring-2024).

<div align="center">

[![PythonSupported](https://img.shields.io/badge/python-3.8-brightgreen.svg)](https://python3statement.org/#sections50-why)
[![Pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://pre-commit.com/)

</div>


## Table of contents
- [Table of contents](#table-of-contents)
- [Repository contents](#repository-contents)
- [Repository Methodology](#repository-methodology)
- [System requirements](#system-requirements)
- [Getting Started](#getting-started)
- [Contributing](#contributing)
- [Contacts](#contacts)


## Repository contents

- [configs](configs) - configuration files directory
- [mlops3_spring_2024](mlops3_spring_2024) - source files of the project
- [notebooks](notebooks) - directory for `jupyter` notebooks
- [scripts](scripts) - repository service scripts
  > These ones are not included into the package if you build one - these scripts are only for usage of repository
- [tests](tests) - project tests based on [pytest](https://docs.pytest.org/en/stable/)
- [.editorconfig](.editorconfig) - configuration for [editorconfig](https://editorconfig.org/)
- [.flake8](.flake8) - [flake8](https://github.com/pycqa/flake8) linter configuration
- [.gitignore](.gitignore) - the files/folders `git` should ignore
- [.pre-commit-config.yaml](.pre-commit-config.yaml) - [pre-commit](https://pre-commit.com/) configuration file
- [CONTRIBUTING.md](CONTRIBUTING.md) - guide for development team
- [pyproject.toml](pyproject.toml) - Python project configuration
- [README.md](README.md) - the one you read =)


## Repository Methodology

This repository follows a structured approach to ensure efficient and organized development:

- **Branching Strategy**: This project employs an "Experimentation and Iteration" branching strategy, tailored for data science workflows. Each branch represents a distinct analytical experiment, data exploration phase, or model iteration, allowing for parallel development of various analysis streams without affecting the stable, main codebase.

- **Issue Tracking**: We track tasks, bugs, and feature requests using GitLab's issue tracker. Please check existing issues before opening a new one.

- **Pull Requests**: Contributions are made through pull requests. Each pull request should be linked to an issue. Make sure your PR description clearly describes the problem and solution, including any relevant issue numbers.

- **Code Reviews**: All submissions require review and approval from at least one project maintainer. This ensures code quality and consistency.


## System requirements

- Python version: 3.8
- Operating system: Ubuntu 22.04 / MacOS 13.4.1
  > Other versions or operating systems may be compatible but have not been tested.


## Getting Started

1. Clone the repo: `git clone --recurse-submodules git@gitlab.com:yagafarova/mlops3-course-spring-2024.git`

2. Install requirements (preferably in virtual environment): `pip install .` or `poetry install`, etc.

3. Add necessary environment variables to `.env`.

4. Run scripts in `scripts` directory.


## Contributing

We welcome contributions! Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.


## Contacts
For any questions, please feel free to use [GitLab issues](https://gitlab.com/yagafarova/mlops3-course-spring-2024/-/issues) or contact [Kseniia Alekseitseva](mailto:xeniayagafarova@gmail.com) directly.
